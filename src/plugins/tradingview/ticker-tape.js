import { useEffect, useState } from 'react';

export default function TickerTape() {    
    const [loaded, setLoaded] = useState(false);  

    useEffect(() => {
        if( !loaded ) {
            setLoaded(true)
            const script = document.createElement('script');
            script.src = 'https://s3.tradingview.com/external-embedding/embed-widget-ticker-tape.js'
            script.async = true;
            script.innerHTML = JSON.stringify({
                "symbols": [
                    {
                        "proName": "FOREXCOM:SPXUSD",
                        "title": "S&P 500"
                    },
                    {
                        "proName": "FOREXCOM:NSXUSD",
                        "title": "US 100"
                    },
                    {
                        "proName": "FX_IDC:EURUSD",
                        "title": "EUR/USD"
                    },
                    {
                        "proName": "BITSTAMP:BTCUSD",
                        "title": "BTC/USD"
                    },
                    {
                        "proName": "BITSTAMP:ETHUSD",
                        "title": "ETH/USD"
                    }
                ],
                "showSymbolLogo": true,
                "colorTheme": "light",
                "isTransparent": false,
                "displayMode": "adaptive",
                "locale": "en"
            }
            )
            document.getElementById("ticker-tape").appendChild(script);
        }
    }, [loaded])

    return (
        <div className="tradingview-widget-container">
            <div className="tradingview-widget-container__widget" id="ticker-tape">                
            </div>            
        </div>
    ) 
}