import React, { useState, useEffect } from 'react';
import useWebSocket from 'react-use-websocket';
import { getEventDetail } from '../data/parse-meta';

export default function Guard(props) {
    const [eventDetail, setEventDetail] = useState(null)
    const [guardUrl, setGuardUrl] = useState(null)

    useEffect(() => {
        (async () => {
            try {
                let respEventDetail = await getEventDetail(props.slug);
                console.log("respEventDetail", respEventDetail)
                setEventDetail(respEventDetail)
                setGuardUrl(respEventDetail["data"]["guard_url"])
            } catch (error) {
                console.log(error)
            } finally {
            }
        })()
    }, [props.slug])

    const {
        sendJsonMessage
    } = useWebSocket(guardUrl, {
        onOpen: () => {
            console.log('vanguard opened')
            sendJsonMessage(
                { "action_type": "join_chat_room", "username": "vinsen", "iat": 1589535520, "recon": false }
            )
        },
        onMessage: (e) => {
            const objData = JSON.parse(e.data);
            if (objData['action_type'] === 'join_chat_success') {
                console.log("joinchat ok", {
                    'room_id': objData.room_id,
                    'token': objData.token,
                })

                props.onJoinChatSuccess({
                    'room_id': objData.room_id,
                    'token': objData.token,
                })
            }
        },
        shouldReconnect: (closeEvent) => true,
    });


    return (<></>);
};
