import { useEffect, useState } from 'react';
import Container from '@mui/material/Container';

const ChartSettings = [
    {
        "width": 860,
        "height": 400,
        "symbolsGroups": [
            {
                "name": "Stock",
                "symbols": [
                    {
                        "name": "IDX:BBCA",
                        "displayName": "BBCA"
                    },
                    {
                        "name": "IDX:BBRI",
                        "displayName": "BBRI"
                    },
                    {
                        "name": "IDX:TLKM",
                        "displayName": "TLKM"
                    },
                    {
                        "name": "IDX:BMRI",
                        "displayName": "BMRI"
                    },
                    {
                        "name": "IDX:UNVR",
                        "displayName": "UNVR"
                    },
                    {
                        "name": "IDX:ASII",
                        "displayName": "ASII"
                    }
                ]
            }
        ],
        "showSymbolLogo": true,
        "colorTheme": "light",
        "isTransparent": false,
        "locale": "en"
    },
    {
        "width": 860,
        "height": 400,
        "symbolsGroups": [
            {
                "name": "Indices",
                "originalName": "Indices",
                "symbols": [
                    {
                        "name": "IDX:COMPOSITE",
                        "displayName": "IHSG"
                    },
                    {
                        "name": "IDX:IDX30",
                        "displayName": "IDX30"
                    },
                    {
                        "name": "IDX:LQ45",
                        "displayName": "LQ45"
                    },
                    {
                        "name": "FOREXCOM:SPXUSD",
                        "displayName": "S&P 500"
                    },
                    {
                        "name": "FOREXCOM:DJI",
                        "displayName": "Dow 30"
                    }
                ]
            }
        ],
        "showSymbolLogo": true,
        "colorTheme": "light",
        "isTransparent": false,
        "locale": "en"
    },
    {
        "width": 860,
        "height": 400,
        "symbolsGroups": [
            {
                "name": "Forex",
                "originalName": "Forex",
                "symbols": [
                    {
                        "name": "FX_IDC:IDRUSD",
                        "displayName": "IDR/USD"
                    },
                    {
                        "name": "FX_IDC:IDRSGD",
                        "displayName": "IDR/SGD"
                    },
                    {
                        "name": "FX_IDC:IDREUR",
                        "displayName": "IDR/EUR"
                    },
                    {
                        "name": "FX_IDC:IDRAUD",
                        "displayName": "IDR/AUD"
                    },
                    {
                        "name": "FX_IDC:USDCNY",
                        "displayName": "USD/CNY"
                    },
                    {
                        "name": "FX:EURUSD",
                        "displayName": "EUR/USD"
                    }
                ]
            }
        ],
        "showSymbolLogo": true,
        "colorTheme": "light",
        "isTransparent": false,
        "locale": "en"
    },
    {
        "width": 860,
        "height": 400,
        "symbolsGroups": [
            {
                "name": "Crypto",
                "symbols": [
                    {
                        "name": "BINANCE:BTCUSDT",
                        "displayName": "BTCUSDT"
                    },
                    {
                        "name": "BINANCE:ETHUSDT",
                        "displayName": "ETH/USDT"
                    },
                    {
                        "name": "BINANCE:BNBUSDT",
                        "displayName": "BNB/USDT"
                    },
                    {
                        "name": "BINANCE:SOLUSDT",
                        "displayName": "SOL/USDT"
                    },
                    {
                        "name": "BINANCE:ADAUSDT",
                        "displayName": "ADA/USDT"
                    }
                ]
            }
        ],
        "showSymbolLogo": true,
        "colorTheme": "light",
        "isTransparent": false,
        "locale": "en"
    },
    {
        "width": 860,
        "height": 400,
        "symbolsGroups": [
            {
                "name": "Commodities",
                "symbols": [
                    {
                        "name": "TVC:GOLD",
                        "displayName": "GOLD"
                    },
                    {
                        "name": "TVC:SILVER",
                        "displayName": "SILVER"
                    },
                    {
                        "name": "TVC:UKOIL",
                        "displayName": "UKOIL"
                    },
                    {
                        "name": "CURRENCYCOM:COPPER",
                        "displayName": "COPPER"
                    }
                ]
            }
        ],
        "showSymbolLogo": true,
        "colorTheme": "light",
        "isTransparent": false,
        "locale": "en"
    }
];

export default function MarketQuotes(props) {
    const { type, displayed } = props;
    const [loaded, setLoaded] = useState(false);

    useEffect(() => {
        if (!loaded) {
            setLoaded(true)
            const script = document.createElement('script');
            script.src = 'https://s3.tradingview.com/external-embedding/embed-widget-market-quotes.js';
            script.async = true;
            script.innerHTML = JSON.stringify(ChartSettings[type]);
            document.getElementById(`market-quotes${type}`).innerHTML = "";
            document.getElementById(`market-quotes${type}`).appendChild(script);
        }
    }, [loaded])

    return (
        <Container maxWidth="xl" style={{ display: displayed ? "block" : "none" }} >
            <Container maxWidth="md" id={`market-quotes${type}`}>
            </Container>
            <div className="tradingview-widget-copyright">
                <div className="tradingview-widget-copyright">
                    <a href="https://www.tradingview.com/markets/indices/" rel="noopener" target="_blank"><span className="blue-text">Widget</span></a> by TradingView
                </div>
            </div>
        </Container>
    );
}