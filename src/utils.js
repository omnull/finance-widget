import { useEffect, useRef } from 'react';

export function useInterval(callback, delay) {
  const savedCallback = useRef();

  useEffect(() => {
    savedCallback.current = callback;
  }, [callback]);

  useEffect(() => {
    function tick() {
      savedCallback.current();
    }

    if (delay !== null) {
      let id = setInterval(tick, delay);
      return () => clearInterval(id);
    }
  }, [delay]);
}

var currAudio;

export function playSound(audio, callback) {  
  audio.play();
  currAudio = audio;
  if (callback) {    
    audio.addEventListener('ended', callback);
  }
}

export function playSoundQueue(sounds) {
  var index = 0;
  function recursivePlay() {
    if (index + 1 === sounds.length) {
      playSound(sounds[index], null);
    } else {      
      playSound(sounds[index], function() {
        index++;
        recursivePlay();
      });
    }
  }
  recursivePlay();
}

export function pauseSound() {
  currAudio.pause();
}

export function resumeSound() {
  currAudio.play();
}