import axios from 'axios'
import {BASE_URL} from '../config/constant'

export const parseMetaData = async (url) => {
    console.log(BASE_URL)
    const res = await axios.post(
      `${BASE_URL}/metadata/parse`,
      {'url':url}
    )
    return res.data
}

export const getSpeeches = async () => {
  console.log(BASE_URL)
  const res = await axios.get(
    `${BASE_URL}/speeches`
  )
  return res.data
}

export const getEventDetail = async (slug) => {
  console.log(BASE_URL)
  const res = await axios.get(
    `${BASE_URL}/event/${slug}`
  )
  return res.data
}