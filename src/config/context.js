import React, { Component } from "react";
const MyContext = React.createContext();
const { Provider, Consumer } = MyContext;

class ConfigProvider extends Component {
   
  logout  = () => {
    localStorage.removeItem("token");
    localStorage.removeItem("uid");
    localStorage.removeItem("username");

    this.setState({ token: null, uid:null, username: null });
  }

  setToken = strToken => {
    if( strToken === null ) {
      this.logout();
    } else {
      localStorage.setItem("token", strToken);
      this.setState({ token: strToken });
    }
  }; 

  constructor(props) {
    super(props);     
    let storedToken = localStorage.getItem("token");
    let storedUid = localStorage.getItem("uid");
    let storedUsername = localStorage.getItem("username");

    this.state = {
      token: storedToken,
      uid: storedUid,      
      username: storedUsername,
      setToken: this.setToken, 
      logout: this.logout
    };
  }

  render() {
    return <Provider value={this.state}>{this.props.children}</Provider>;
  }
}

export { MyContext };
export { ConfigProvider };
export { Consumer };
