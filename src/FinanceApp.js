import './App.css';
import React, { useEffect, useState } from 'react';
import { useParams } from "react-router-dom";
import { styled } from '@mui/material/styles';
import PropTypes from 'prop-types';
import Stack from '@mui/material/Stack';
import Stepper from '@mui/material/Stepper';
import Step from '@mui/material/Step';
import StepLabel from '@mui/material/StepLabel';
import TickerTape from './plugins/tradingview/ticker-tape';
import MarketQuotes from './plugins/tradingview/market-quotes';
import Chat from './plugins/chat';
import StepConnector, { stepConnectorClasses } from '@mui/material/StepConnector';
import PublicIcon from '@mui/icons-material/Public';
import AttachMoneyIcon from '@mui/icons-material/AttachMoney';
import TimelineIcon from '@mui/icons-material/Timeline';
import EvStationOutlinedIcon from '@mui/icons-material/EvStationOutlined';
import WaterfallChartIcon from '@mui/icons-material/WaterfallChart';
import WebMeta from './plugins/ads/web-meta';
import Fab from '@mui/material/Fab';
import Box from '@mui/material/Box';
import PlayCircleFilledWhiteOutlinedIcon from '@mui/icons-material/PlayCircleFilledWhiteOutlined';
import StopCircleOutlinedIcon from '@mui/icons-material/StopCircleOutlined';
import HourglassBottomIcon from '@mui/icons-material/HourglassBottom';
import { getSpeeches } from './data/parse-meta'
import { useInterval, playSoundQueue, pauseSound, resumeSound } from './utils'

const ColorlibConnector = styled(StepConnector)(({ theme }) => ({
  [`&.${stepConnectorClasses.alternativeLabel}`]: {
    top: 22,
  },
  [`&.${stepConnectorClasses.active}`]: {
    [`& .${stepConnectorClasses.line}`]: {
      backgroundImage:
        'linear-gradient( 95deg,rgb(242,113,33) 0%,rgb(233,64,87) 50%,rgb(138,35,135) 100%)',
    },
  },
  [`&.${stepConnectorClasses.completed}`]: {
    [`& .${stepConnectorClasses.line}`]: {
      backgroundImage:
        'linear-gradient( 95deg,rgb(242,113,33) 0%,rgb(233,64,87) 50%,rgb(138,35,135) 100%)',
    },
  },
  [`& .${stepConnectorClasses.line}`]: {
    height: 3,
    border: 0,
    backgroundColor:
      theme.palette.mode === 'dark' ? theme.palette.grey[800] : '#eaeaf0',
    borderRadius: 1,
  },
}));

const ColorlibStepIconRoot = styled('div')(({ theme, ownerState }) => ({
  backgroundColor: theme.palette.mode === 'dark' ? theme.palette.grey[700] : '#ccc',
  zIndex: 1,
  color: '#fff',
  width: 38,
  height: 38,
  display: 'flex',
  borderRadius: '50%',
  justifyContent: 'center',
  alignItems: 'center',
  ...(ownerState.active && {
    backgroundImage:
      'linear-gradient( 136deg, rgb(242,113,33) 0%, rgb(233,64,87) 50%, rgb(138,35,135) 100%)',
    boxShadow: '0 4px 10px 0 rgba(0,0,0,.25)',
  }),
  ...(ownerState.completed && {
    backgroundImage:
      'linear-gradient( 136deg, rgb(242,113,33) 0%, rgb(233,64,87) 50%, rgb(138,35,135) 100%)',
  }),
}));

function ColorlibStepIcon(props) {
  const { active, completed, className } = props;

  const icons = {
    1: <WaterfallChartIcon />,
    2: <TimelineIcon />,
    3: <AttachMoneyIcon />,    
    4: <PublicIcon />,
    5: <EvStationOutlinedIcon />,
  };

  return (
    <ColorlibStepIconRoot ownerState={{ completed, active }} className={className}>
      {icons[String(props.icon)]}
    </ColorlibStepIconRoot>
  );
}

ColorlibStepIcon.propTypes = {
  /**
   * Whether this step is active.
   * @default false
   */
  active: PropTypes.bool,
  className: PropTypes.string,
  /**
   * Mark the step as completed. Is passed to child components.
   * @default false
   */
  completed: PropTypes.bool,
  /**
   * The label displayed in the step icon.
   */
  icon: PropTypes.node,
};

export default function FinanceApp( { match } ) {
  let params = useParams();
  const SoundStoped = 0;
  const SoundStarted = 1;
  const SoundPaused = 2;
  const steps = ['Stock', 'Index', 'Currency', 'Crypto', 'Commodities'];
  const [activeStep, setActiveStep] = useState(0);
  const [soundStatus, setSoundStatus] = useState(SoundStoped);
  const [chatConnected, setChatConnected] = useState(false);
  const [speeches, setSpeeches] = useState([]);
  const [ads, setAds] = useState({"url":"", "start":0});
  const [adsQueue, setAdsQueue] = useState( [] );
  

  useEffect(() => {
    (async () => {
      try {
        let respSpeeches = await getSpeeches();
        console.log("speeches", respSpeeches.speeches)
        setSpeeches(respSpeeches.speeches)
      } catch (error) {
        console.log(error)
      } finally {
      }
    })()
  }, []);

  useInterval(() => {
    let currAds = ads
    if( currAds.start !== 0 && new Date().getTime() - currAds.start > 15 * 1000 ) {
      currAds = {"url":"", "start": 0}
      setAds(currAds)      
    }

    if( currAds.start === 0 && adsQueue.length > 0 ) {      
      const nextAds = {"url":adsQueue.pop(), "start": new Date().getTime()};
      setAds(nextAds)
      setAdsQueue(adsQueue)
    }

    var nextSteps = activeStep + 1;
    nextSteps = nextSteps % steps.length;        
    setActiveStep(nextSteps);        
  }, 5000);  

  const isValidHttpUrl = (string) => {
    let url;
    console.log(":string", string)
    try {
      url = new URL(string);
    } catch (_) {
      return false;
    }
    console.log(":url", url)
    return url.protocol === "http:" || url.protocol === "https:";
  }

  const onChatConnected = (e) => {
    setChatConnected(true)
  }

  const onGift = (e) => {
    console.log("onGift", e)
    if (e.message !== "" && isValidHttpUrl(e.message)) {
      adsQueue.push( e.message )
      setAdsQueue( adsQueue )
    }
  }

  const onChat = (e) => {
    //if (e.msg !== "" && isValidHttpUrl(e.msg)) {
      //setAdsUrl(e.msg)
    //}
  }

  const onFabClicked = (e) => {    
    const currentSoundStatus = soundStatus; 
    switch(currentSoundStatus){
      case SoundStoped:
        if(speeches.length > 0 ) {
          let arrAudio = []
          for( const speech of speeches ){
            arrAudio.push( new Audio(speech.url) )
          }
          playSoundQueue( arrAudio )
          setSoundStatus(SoundStarted)
        }
        break
      case SoundStarted:
        pauseSound()
        setSoundStatus(SoundPaused)
        break;
      case SoundPaused:
        resumeSound()
        setSoundStatus(SoundStarted)
        break;
    }
  }
  
  return (
    <Box maxWidth="xl">
      <Chat 
        slug={params.slug}
        onChatConnected={onChatConnected} 
        onGift={onGift} 
        onChat={onChat} />      

      <Stack sx={{ width: '100%' }} spacing={4}>
        {ads.url === "" ? <TickerTape  /> : <WebMeta url={ads.url} />}
        <MarketQuotes type={0} displayed={activeStep === 0} />
        <MarketQuotes type={1} displayed={activeStep === 1} />
        <MarketQuotes type={2} displayed={activeStep === 2} />
        <MarketQuotes type={3} displayed={activeStep === 3} />
        <MarketQuotes type={4} displayed={activeStep === 4} />
        {
          <Stepper nonLinear alternativeLabel activeStep={activeStep} connector={<ColorlibConnector />}>
            {
              steps.map((label) => (
                <Step key={label}>
                  <StepLabel StepIconComponent={ColorlibStepIcon}>{label}</StepLabel>
                </Step>
              ))
            }
          </Stepper>
        }
      </Stack>

      <Fab variant="extended" size="small" color="primary" onClick={onFabClicked}>
        {soundStatus === SoundStarted ?
          <StopCircleOutlinedIcon sx={{ mr: 1 }} /> :
          speeches.length > 0 && chatConnected ?
            <PlayCircleFilledWhiteOutlinedIcon sx={{ mr: 1 }} /> :
            <HourglassBottomIcon sx={{ mr: 1 }} />
        }
      </Fab>
    </Box>
  );
}
