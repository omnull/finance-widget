export const BASE_URL = process.env.REACT_APP_BASE_URL
export const BASE_APP_PATH = process.env.REACT_APP_PATH
export const BASE_THEME_PRIMARY = process.env.REACT_APP_THEME_PRIMARY
export const BASE_DASHBOARD_PATH = 'dashboard'
export const LANGUAGES = {"en":"English", "id":"Bahasa"}
