import React, { useState, useEffect } from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { parseMetaData } from '../../data/parse-meta';
import QRCode from "react-qr-code";
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';

export default function WebMeta(props) {
  const [metadata, setMetadata] = useState({
    description: "",
    image: "",
    title: "",
  })

  useEffect(() => {
    (async () => {
      try {
        let respMetaData = await parseMetaData(props.url);
        if (respMetaData.title.length > 50) {
          respMetaData.title = respMetaData.title.substring(0, 47) + "...";
        }
        if (respMetaData.description.length > 150) {
          respMetaData.description = respMetaData.description.substring(0, 147) + "...";
        }
        setMetadata(respMetaData)
      } catch (error) {
        console.log(error)
      } finally {
      }
    })()
  }, [props.url])

  return (
    <Container maxWidth="xl" style={{ display: "block", height: "102" }} >
      <Container maxWidth="md">
        <Card sx={{ display: 'flex' }} >
          <Box sx={{ display: 'flex'}}>
            <Box >
              {metadata.image === "" ? 
                <QRCode value={props.url} size={101} />:
                <CardMedia
                  component="img"
                  sx={{ width: 101, height: 101 }}
                  src={metadata.image} />
              }
            </Box>
            <Box sx={{ display: 'flex', flexDirection: 'column', flexGrow: 1 }}>
              <CardContent sx={{ flex: '1 0 auto' }}>
                <Typography component="div" variant="h5">
                  {metadata.title}
                </Typography>
                <Typography variant="subtitle1" color="text.secondary" component="div">
                  {metadata.description === "" ?props.url:metadata.description}
                </Typography>
              </CardContent>
            </Box>
            <Box >
              <QRCode value={props.url} size={101} />
            </Box>
          </Box>
        </Card>
      </Container>
    </Container>
  );
}