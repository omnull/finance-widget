import React, { useState, useEffect } from 'react';
import useWebSocket, { ReadyState } from 'react-use-websocket';
import Guard from './guard';

export default function Chat(props) {
    const socketUrl = 'wss://g-gschat.goplay.co.id/chat';
    const [authMessage, setaAuthMessage] = useState(null)

    const onMessage = (e) => {
        if( e.data === 'null' ) {
            return
        }

        const data = JSON.parse(e.data)
        if( data['ct'] === 10 ) {
            props.onChatConnected(data)
        } else if( data['ct'] === 20 ) {
            props.onChat(data)
        } else if( data['ct'] === 82 ) {
            props.onGift(data)
        }
    }

    const {
        sendMessage,
        lastJsonMessage,
        readyState,
    } = useWebSocket(socketUrl, {
        onOpen: () => console.log('opened'),
        onMessage: onMessage,
        shouldReconnect: (closeEvent) => true,
    });    

    const connectionStatus = {
        [ReadyState.CONNECTING]: 'Connecting',
        [ReadyState.OPEN]: 'Open',
        [ReadyState.CLOSING]: 'Closing',
        [ReadyState.CLOSED]: 'Closed',
        [ReadyState.UNINSTANTIATED]: 'Uninstantiated',
    }[readyState];

    useEffect(() => {
        if (readyState === ReadyState.OPEN) {
            sendMessage(authMessage)
        }
    }, [readyState, authMessage])   

    const onJoinChatSuccess = ( {room_id, token} ) => {
        setaAuthMessage(
            JSON.stringify(
                {
                    "ct":10,
                    "room_id": room_id,
                    "token": token,
                    "recon":false
                }
            )
        )
    }

    return (
        <Guard 
            slug={props.slug}
            onJoinChatSuccess={onJoinChatSuccess}/>
    );
};
 